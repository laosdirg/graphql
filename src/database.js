let NEXT_ID = 1337;

export const MEM_DB = {
  users: {
    123: {
      name: "Zacharias Knudsen",
      isVegetarian: false
    },
    321: {
      name: "Emil Johansen",
      isVegetarian: false
    }
  },
  groups: {
    456: {
      name: "Top Danmark",
      createdAt: "2018-01-01T13:37:00.000Z"
    },
    567: {
      name: "Bytes & Brains",
      createdAt: "2018-01-01T13:37:00.000Z"
    }
  },
  dinners: {
    42: {
      price: 98.76,
      servedAt: "2018-04-23T18:30:00.000Z",
      chefId: 321,
      groupId: 456
    },
    43: {
      price: 12.76,
      servedAt: "2018-04-23T18:30:00.000Z",
      chefId: 123,
      groupId: 567
    }
  },
  groups_users: [
    {
      userId: 123,
      groupId: 456
    },
    {
      userId: 321,
      groupId: 456
    },
    {
      userId: 123,
      groupId: 567
    },
    {
      userId: 321,
      groupId: 567
    }
  ]
};

// simulating synchronous database operations
const database = {
  getUserGroupIds(id) {
    return MEM_DB.groups_users
      .filter(item => item.userId == id)
      .map(item => item.groupId);
  },
  getGroupDinnerIds(id) {
    return Object.entries(MEM_DB.dinners)
      .filter(([key, value]) => value.groupId == id)
      .map(([key, value]) => key);
  },
  getGroupMemberIds(id) {
    return MEM_DB.groups_users
      .filter(item => item.groupId == id)
      .map(item => item.userId);
  },
  getUser(id) {
    const user = MEM_DB.users[id];
    return { id: parseInt(id, 10), ...user };
  },
  getGroup(id) {
    const group = MEM_DB.groups[id];
    return { id: parseInt(id, 10), ...group };
  },
  getDinner(id) {
    const dinner = MEM_DB.dinners[id];
    return { id: parseInt(id, 10), ...dinner };
  },
  allUsers() {
    return Object.keys(MEM_DB.users).map(database.getUser);
  },
  allGroups() {
    return Object.keys(MEM_DB.groups).map(database.getGroup);
  },
  allDinners() {
    return Object.keys(MEM_DB.dinners).map(database.getDinner);
  },
  createDinner({ chefId, groupId, price, servedAt }) {
    const id = NEXT_ID++;

    const dinner = {
      chefId,
      groupId,
      price,
      servedAt
    };

    MEM_DB.dinners[id] = dinner;

    return id;
  }
};

export default database;
