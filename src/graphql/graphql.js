import { ApolloServer } from "apollo-server-express";

import * as CreateDinner from "./types/CreateDinner.js";
import * as Dinner from "./types/Dinner.js";
import * as Group from "./types/Group.js";
import * as Mutation from "./types/Mutation.js";
import * as Query from "./types/Query.js";
import * as User from "./types/User.js";

const types = [CreateDinner, Dinner, Group, Mutation, Query, User];

export default new ApolloServer({
  typeDefs: types.map(type => type.typeDefs),
  resolvers: types.map(type => type.resolvers),
  context: ({ req }) => req
});
