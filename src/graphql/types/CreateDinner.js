const { gql } = require("apollo-server-express");
import database from "../../database.js";

export const typeDefs = gql`
  input CreateDinnerInput {
    chefId: Int!
    groupId: Int!
    price: Float!
    servedAt: String!
  }

  type CreateDinnerPayload {
    dinner: Dinner
  }

  extend type Mutation {
    createDinner(input: CreateDinnerInput!): CreateDinnerPayload!
  }
`;

export const resolvers = {
  CreateDinnerPayload: {
    // get the newly created dinner using id from mutation resolver
    dinner(previousObject, args, context) {
      const { dinnerId } = previousObject;
      return database.getDinner(dinnerId);
    }
  },
  Mutation: {
    // create a dinner and return id
    createDinner(rootValue, args, context) {
      const dinnerId = database.createDinner(args.input);
      // we only return id, which can be used
      // by resolvers further down the tree
      return { dinnerId };
    }
  }
};
