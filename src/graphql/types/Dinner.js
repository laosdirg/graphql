const { gql } = require("apollo-server-express");
import database from "../../database.js";

export const typeDefs = gql`
  """
  A dinner hosted by a chef for a group of persons
  """
  type Dinner {
    id: Int
    price: Float
    servedAt: String
    chef: User
    group: Group
  }

  extend type Query {
    dinner(id: Int): Dinner
    dinners: [Dinner]
  }
`;

export const resolvers = {
  Dinner: {
    // get the chef of this dinner
    chef(previousObject, args, context) {
      const dinner = previousObject;
      return database.getUser(dinner.chefId);
    },

    // get the group this dinner is hosted for
    group(previousObject, args, context) {
      const dinner = previousObject;
      return database.getGroup(dinner.groupId);
    }
  },
  Query: {
    dinner(rootValue, args, context) {
      return database.getDinner(args.id);
    },
    dinners(rootValue, args, context) {
      return database.allDinners();
    }
  }
};
