const { gql } = require("apollo-server-express");
import database from "../../database.js";

export const typeDefs = gql`
  """
  A group of persons who go to dinner clubs together
  """
  type Group {
    id: Int
    name: String
    createdAt: String
    members: [User]
    dinners: [Dinner]
  }

  extend type Query {
    group(id: Int): Group
    groups: [Group]
  }
`;

export const resolvers = {
  Group: {
    // get all members of this group
    members(previousObject, args, context) {
      const group = previousObject;
      const memberIds = database.getGroupMemberIds(group.id);
      return memberIds.map(database.getUser);
    },

    // get all dinners hosted for this group
    dinners(previousObject, args, context) {
      const group = previousObject;
      const dinnerIds = database.getGroupDinnerIds(group.id);
      return dinnerIds.map(database.getDinner);
    }
  },
  Query: {
    group(rootValue, args, context) {
      return database.getGroup(args.id);
    },
    groups(rootValue, args, context) {
      return database.allGroups();
    }
  }
};
