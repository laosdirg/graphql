const { gql } = require("apollo-server-express");

export const typeDefs = gql`
  """
  Root Query Type, defines entrypoints into API
  """
  type Query {
    headers: String
  }
`;

export const resolvers = {
  Query: {
    headers(rootValue, args, context) {
      return JSON.stringify(context.headers);
    }
  }
};
