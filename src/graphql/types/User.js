const { gql } = require("apollo-server-express");
import database from "../../database.js";

export const typeDefs = gql`
  """
  Person who can join groups and dinners
  """
  type User {
    id: Int
    name: String
    isVegetarian: Boolean
    groups: [Group]
  }

  extend type Query {
    user(id: Int): User
    users: [User]
  }
`;

export const resolvers = {
  User: {
    // fetch all groups for user
    groups(previousObject, args, context) {
      const user = previousObject;
      const groupIds = database.getUserGroupIds(user.id);
      return groupIds.map(database.getGroup);
    }
  },
  Query: {
    user(rootValue, args, context) {
      return database.getUser(args.id);
    },
    users(rootValue, args, context) {
      return database.allUsers();
    }
  }
};
