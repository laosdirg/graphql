const PROPS = {
  group: "group",
  user: "user",
  member: "user",
  dinner: "dinner",
  chef: "user"
};

const REST_ROOT = "http://localhost:3000/rest";

export const serialize = object => {
  Object.entries(PROPS).forEach(([prop, type]) => {
    if (object[`${prop}Ids`]) {
      object[`${prop}s`] = object[`${prop}Ids`].map(
        id => `${REST_ROOT}/${type}s/${id}`
      );
      delete object[`${prop}Ids`];
    }
    if (object[`${prop}Id`]) {
      object[`${prop}`] = `${REST_ROOT}/${type}s/${object[`${prop}Id`]}`;
      delete object[`${prop}Id`];
    }
  });
  return object;
};
