import { Router } from "express";
import database from "../database.js";
import { serialize } from "./helpers.js";

const router = Router();

// rest endpoints

router.get("/users", (req, res) => {
  const users = database.allUsers().map(user => {
    const groupIds = database.getUserGroupIds(user.id);
    return { ...user, groupIds };
  });
  res.send(users.map(serialize));
});

router.get("/users/:id", (req, res) => {
  const { id } = req.params;
  const user = database.getUser(id);
  const groupIds = database.getUserGroupIds(id);
  res.send(serialize({ ...user, groupIds }));
});

router.get("/groups", (req, res) => {
  const groups = database.allGroups().map(group => {
    const memberIds = database.getGroupMemberIds(group.id);
    const dinnerIds = database.getGroupDinnerIds(group.id);
    return { ...group, dinnerIds, memberIds };
  });
  res.send(groups.map(serialize));
});

router.get("/groups/:id", (req, res) => {
  const { id } = req.params;
  const group = database.getGroup(id);
  const memberIds = database.getGroupMemberIds(id);
  const dinnerIds = database.getGroupDinnerIds(id);
  res.send(serialize({ ...group, dinnerIds, memberIds }));
});

router.get("/dinners", (req, res) => {
  const dinners = database.allDinners();
  res.send(dinners.map(serialize));
});

router.get("/dinners/:id", (req, res) => {
  const dinner = database.getDinner(req.params.id);
  res.send(serialize(dinner));
});

// ad-hoc endpoints

router.get("/userDashboard/:id", (req, res) => {
  const { id } = req.params;
  const user = database.getUser(id);
  const groupIds = database.getUserGroupIds(id);
  const groups = groupIds.map(database.getGroup).map(group => {
    const dinnerIds = database.getGroupDinnerIds(group.id);
    const dinners = dinnerIds.map(database.getDinner);
    return { ...group, dinners };
  });
  res.send(serialize({ ...user, groups }));
});

export default router;
