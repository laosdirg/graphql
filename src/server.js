import express from "express";

import graphql from "./graphql/graphql.js";
import rest from "./rest/rest.js";

// Initialize the server
const server = express();

// Mount the GraphQL endpoint
graphql.applyMiddleware({ app: server });

// Mount the REST endpoint
server.use("/rest", rest);

export default server;
