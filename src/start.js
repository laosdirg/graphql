import server from "./server.js";

// Start the server
const listener = server.listen(3000, () => {
  const { port } = listener.address();
  console.log(`🚀 GraphQL server ready at http://localhost:${port}/graphql`);
  console.log(`🚀 REST server ready at http://localhost:${port}/rest`);
});
